#import file_types
#def archive(filename, temp_dir)
from office_lib import docx2text

extensions = ['docx', ]


def get_contents(f, data):
    try:
        data.body = docx2text(f)
    except:
        print 'error in DOCX file'
    return data

