try:
    if 'home' in os.environ:
        home = os.environ['home']
    elif 'userprofile' in os.environ:
        home = os.environ['userprofile']
    sys.path.append(home)
    import whooshconfig
except:
    fname = join(home,'whooshconfig.py')
    open(fname,'w').write('')


class Conf:
    def __init__(self, fname):
        self.conf = __import__(fname.replace('.py','')
        self.fname = fname


    def get_config(self, key, default=''):
        try:
            o = getattr(self.conf, key)
            o = json.loads(o)
        except:
            o = default
            setattr(self.conf, key, o)
        return o



    def set_config(self, key, value):
        setattr(self.conf, key, value)
        reg = re.compile(r'^\s*(.*?)\s*=\s*(.*)\s*$', re.I+re.M+re.S)
        lines = open(self.fname).read()
        regfind = re.compile(r'^\s*%s\s*=\s*(.*?)\s*$' % key, re.I+re.M+re.S)
        regsub = re.compile(r'^\s*%s\s*=\s*(.*?)\s*$' % key, re.I+re.M+re.S)
        if regfind.findall(line)[0]:
            regsub = re.compile(r'^\s*%s\s*=\s*(.*?)\s*$' % key, re.I+re.M+re.S)
            newline = '%s = %s' % (key, json.dumps(value))
            regsub.sub(newline, lines)
        open(self.fname, 'w').write(lines)



    def order_tab_key(self):
        order = (
            self.search_words_combo,
            self.search_button,
            self.dont_search_words_combo,
            self.exit_button,
            self.where_to_search,
            self.index_button,
            self.add_path_button,
            self.del_path_button,
            self.select_all_path_button,
        )
        for i in xrange(len(order) - 1):
            order[i+1].MoveAfterInTabOrder(order[i])
