from library import err
import zipfile
#import file_types
#def archive(filename, temp_dir)
extensions = ['zip', 'egg', ]

#windows_cmd = ''
#linux_cmd = 'unzip -d $f'
# 'filename' is from the parent scope

def get_contents(f, data):

    try:
        err('parsing zip file')
        z = zipfile.ZipFile(f)
        found_files = z.namelist()
        data.body = u'\n'.join(found_files)
        err('done parsing zip file')
    except:
        err("(not a zip file? - %s)" % f)
    return data
