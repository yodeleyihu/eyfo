'''

EYFO - a local file indexer

contact:    https://bitbucket.org/yodeleyihu/eyfo
license:    LGPL
created:    02/03/2013
version:    0.4 (alpha)

default config file, in case the file is missing or it's a new install

'''

CONF_FILE_BASE_NAME = 'eyfo-config.json'

default_config_values = {
    "filesystem_manager": "c:\\program files\\total commander\\totalcmd.exe",
    'progress_indicator_gif_animation_file': 'progress.gif',
	"editor": "c:\\windows\\system32\\notepad.exe",
	"index_start_folder": "d:\\",
	"whoosh_index_db_dir": "d:\\temp\\whoosh",
    "max_results" : 10000,
    'exclude_patterns' : [
        r'/dev/*',
        r'/proc/*',
        r'/tmp/*',
        r'?:\RECYCLER\*',
        r'*\nltk_data\*',
        r'*\license.txt',
        r'*\readme.txt',
        r'*.tmp',
        r'*.res',
        r'*.swp',
        r'*.pyo',
        r'*.pyc',
        r'*.frx',
        r'?:\windows\AppPatch\*',
        r'?:\windows\Config\*',
        r'?:\windows\Connection Wizard\*',
        r'?:\windows\CSC\*',
        r'?:\windows\Debug\*',
        r'?:\windows\IIS Temporary Compressed Files\*',
        r'?:\windows\inf\*',
        r'?:\windows\Installer\*',
        r'?:\windows\java\*',
        r'?:\windows\l2schemas\*',
        r'?:\windows\Media\*',
        r'?:\windows\Microsoft.NET\*',
        r'?:\windows\Minidump\*',
        r'?:\windows\msagent\*',
        r'?:\windows\msapps\*',
        r'?:\windows\msdownld.tmp\*',
        r'?:\windows\mui\*',
        r'?:\windows\Offline Web Pages\*',
        r'?:\windows\pchealth\*',
        r'?:\windows\Prefetch\*',
        r'?:\windows\Registration\*',
        r'?:\windows\Resources\*',
        r'?:\windows\system32\wbem\*',
        r'?:\windows\Temp\*',
        r'?:\windows\WBEM\*',
        r'?:\windows\Web\*',
        r'?:\windows\WinSxS\*',
    ]
}
