#-*- coding: utf-8 -*-
'''

EYFO - a local file indexer

https://bitbucket.org/yodeleyihu/eyfo
needs a lot of rewriting...
started: 02/03/2013
license:    LGPL
version:    0.4 (alpha)

Whoosh is licensed separately. see whoosh for more info.

'''

import os
from os.path import expanduser, isdir, isfile, join, basename, abspath, splitext
from os import listdir
import sys
import re
import time
from fnmatch import fnmatch as shell_pattern_match
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter as formatter
from library import err



def install_dependencies():
    for i in (1,2):  # try again, maybe interdependencies are already installed
        for modulename in modules:
            try:
                exec('import ' + modulename)
            except:
                if 'l' in modules[modulename]:
                    print "private module '%s' is PRIVATE; it can't be found!"
                    if 'm' in modules[modulename]:
                        print "module: '%s' is mandatory. the program will crash!" % modulename
                        sys.exit(7)
                os.system('pip install ' + modulename)

    for modulename in modules:
        try:
            exec('import ' + modulename)
            installed.append(modulename)
        except:
            if 'm' in modules[modulename]:
                print "module: '%s' is mandatory. the program will crash!" % modulename
                sys.exit(7)
            elif 'o' in modules[modulename]:
                print "module: '%s' cant be insalled and will not be used!" % modulename


installed=[]
modules = {  #   m: mandatory, o: optional, l: local
    'dateutil': 'o',
    'lxml': 'o',
    'xlrd': 'o',
    'docx': 'o',
    'library': 'ml',
}
install_dependencies()


import tempfile
import UserDict
import chardet
from exceptions import KeyboardInterrupt, WindowsError
from library import import_file, PKZIP_PREFIX, decode_filename, filelist_to_html_index
from library import encode_command_arguments, decode_command_arguments, intersection, find_user_dir

from whoosh.index import open_dir
from whoosh.index import create_in
from whoosh.fields import *
from whoosh.qparser import QueryParser
#from html2text import html2text
from whoosh.filedb.filestore import FileStorage
from whoosh.writing import BufferedWriter


class D(UserDict.UserDict):
    def __missing__(self, item):
        return None


def log_type_and_encoding(varname, value):
    print '-------' + varname + '----------'
    print 'type:', type(value),
    enc = convert_encoding(value)
    print ' enc:', enc
    try:
        print value
    except:
        pass
    print '----------------------------------'

def get_file_extension(p):
    ext = os.path.splitext(p)[1]
    if ext in ['.wbk', ]:
        pref = open(p,'rb').read(3)
        if pref==PKZIP_PREFIX:
            ext='.docx'
        else:
            ext='.doc'
    if ext: return ext[1:]
    else: return ''

def convert_encoding(s):
    if len(s)<1:
        return s
    #if s[:2]=='\xff\xfe':
    #    enc = 'utf-16'
    #elif s[:3]=='\xef\xbb\xbf':
    #    enc = 'utf-8'
    #else:
    ret = chardet.detect(s)
    enc = ret['encoding']
    if enc is not None:
        s = unicode(s, enc, 'replace')
    else:
        try: s=unicode(s)
        except: pass
    return s


def quick_hack_remove_junk(s):
    ext = get_file_extension(f)
    return s


def test_plugins(f=''):
    i = Eyfo_index(test=True)
    tests_dir = os.path.split(sys.argv[0])[0]

    if f:
        files = [f]
    else:
        files = [join(TEST_DIR, f) for f in listdir(tests_dir) if f.startswith('test.')]

    for f in files:
        err('================= %s ==========================' % f)
        if not self.file_should_be_indexed(f):
            err('should not be indexed')
            continue
        data = i.add_doc(None, f)
        err('len(body) = %d' % len(data.body))
        if exts[data.ext]:
            item = exts[data.ext][TEST]
            try:
                if item and item not in data.body:
                    err('test is wrong.')
                else:
                    err('test is correct')
            except:
                err('error in test')

def load_plugins( raise_hell=False ):
    #plugins_dir = join(os.path.split(sys.argv[0])[0], PLUGINS_SUB_DIR)
    plugins_dir = os.path.split(sys.argv[0])[0]
    if plugins_dir == '': plugins_dir = '.'
    #if plugins_dir not in sys.path:
    #    sys.path.insert(0, plugins_dir)
    for f in listdir(plugins_dir):
        if f.startswith('plugin_') and f.endswith('.py'):
            f = join(abspath(plugins_dir), f)
            if raise_hell:
                m = import_file(f)
            else:
                try:
                    m = import_file(f)
                except:
                    err('error in import! skipping '+f+'   <--------------<')
                    continue
            #m = __import__(f)
            #import modulename as m
            exts = m.extensions
            func = m.get_contents
            for ext in exts:
                parser_functions[ext] = func
                parseable_exts.append(ext)
            err('loading plugin ' + f + ' : great success!')




class Eyfo_Base:

    def __init__(self, index_db_dir, clean=False, test=False):
        '''  '''
        self.schema = Schema(
            titles=TEXT(stored=True),
            body=TEXT(stored=True),
            path=ID(stored=True),
            filename=TEXT(stored=True),
            type=KEYWORD( lowercase=True, unique=True, stored=True),
            time=DATETIME(stored=True),
            isdir=BOOLEAN(stored=True)
        )
        self.clean = clean
        self.test = test
        self.only_fnames = False
        self.index_db_dir = index_db_dir
        self.root = root_folder
        self.excludes = ['tmp', 'bak', 'swp', 'pyc', 'pyo', ]

        if not self.test:
            self.initialize_index() #creates self.ix


    def initialize_index(self):
        '''  '''
        print 'self.index_db_dir',self.index_db_dir
        if not isdir(self.index_db_dir):
            os.makedirs(self.index_db_dir)
        self.open_or_create_db()


    def open_or_create_db(self):
        '''  '''
        storage = FileStorage(self.index_db_dir)        # Create an index

        if self.clean:
            err('creating a NEW index at:'+ self.index_db_dir)
            self.ix = storage.create_index(self.schema)

        else:
            err('opening index from:'+ self.index_db_dir)
            self.ix = storage.open_index()



class FileData:
    body = u''
    titles = []
    fname = u''
    size = -1
    ext=u''

    def __init__(self, f=''):
        '''  '''
        self.fname=f

    def titlesstr(self):
        '''  '''
        try:
            return u' ; \n'.join([unicode(t) for t in self.titles])
        except:
            return ''

    def __repr__(self):
        '''  '''
        s = clean_text(self.body)[:70] + clean_text(self.titlesstr())[:70]
        return s




def clean_text(s):
    s = s.replace('\n', ' ')
    s = s.replace('  ', ' ')
    s = s.replace('  ', ' ')
    return s

class Eyfo_index(Eyfo_Base):
    '''  '''

    def default_parser(self, f, data):
        '''  '''
        data.body = open(f, 'rb').read()
        data.body = convert_encoding(data.body)


    def add_doc(self, writer, f):
        '''  '''
        data = FileData(f)
        err(f)
        '''try:
            err(' + ' + f)
        except:
            f = guess_encoding(f, ['cp862', 'cp1255', 'utf-8', 'utf-16', 'latin-1'])
            err(' + ' + f)'''

        if data.ext in parseable_exts and not self.only_fnames:
            parser_function = parser_functions[data.ext]
            data.size = os.path.getsize(f)
            err('ext in parseable_exts: ext=%s; size=%db' % (data.ext , data.size ) )

            if data.size<1:
                err('size too small!')

            elif data.ext in exts and \
                 MAXSIZE in exts[data.ext] and \
                 data.size > exts[data.ext][MAXSIZE]*1000000.0:

                 #err('not parseable - too large: ' +str(data.size))
                 pass

            else:
                data = parser_function(f, data)
                #err('parse) len(data.body)=%d' % len(data.body))

        else:
            #err('ext not in parseables')
            #self.default_parser(f, data)
            pass

        #data.body = quick_hack_remove_junk(data.body)
        data.ext = unicode(data.ext)

        try:
            date = os.path.getmtime(f)
        except WindowsError:
            err('windows cant report the date for this file:' + f)
            date = 0

        #f = guess_encoding(f, ['utf-8', 'cp1255', 'latin-1', 'iso8859-8', 'cp862', 'unicode'] )
        #f=unicode(f)

        if not self.test:
            try:
                ddd = apply(datetime.datetime,time.localtime(date)[0:5])
            except:  # who is dating their files to 1781??)
                ddd = apply(datetime.datetime,time.localtime(0)[0:5])
            #print 'adding doc: filename=',data.fname, "titles=", data.titlesstr(), "path=", f, "body=", data.body, "type=", data.ext, "time=", ddd
            writer.add_document(titles=data.titlesstr(), path=f, body=data.body, type=data.ext, time=ddd, filename=data.fname)

        return data




    def file_should_be_indexed(self, f):
        '''  '''
        ext = get_file_extension(f)

        if ext in self.excludes:
            return False

        for m in exclude_patterns:
            if shell_pattern_match( f, m ):
                return False

        return True



    def index_files(self, files_to_index=False, indexed_paths=[]):
        '''  '''
        if self.gui:
            import wx

        try:  # only to catch Ctrl-c

            writer = BufferedWriter(self.ix, period=120, limit=100)

            for root, subFolders, files in os.walk(unicode(self.root)):
                for fname in files:
                    fname = decode_filename(fname)
                    f = join(root, fname)
                    ext = get_file_extension(f)
                    if  (files_to_index is False) \
                        or (f in files_to_index or f not in indexed_paths) \
                        and self.file_should_be_indexed(f):
                        self.add_doc(writer, f)

                    if self.gui:
                        wx.GetApp().Yield(True)  # i wish it was helpful..

                # commit every subdir
                writer.commit()

        except KeyboardInterrupt:
            err('pressed Ctrl-C')

        writer.commit()
        writer.close()



    def incremental_index(self, root_dir='' ):
        '''  '''
        searcher = self.ix.searcher()
        if root_dir:
            if isdir(root_dir):
                self.root = root_dir
            else:
                print 'error! root_dir <%s> is not a directory.' % root_dir
                return False

        # The set of all paths in the index
        indexed_paths = set()
        # The set of all paths we need to re-index
        to_index = set()

        writer = BufferedWriter(self.ix, period=120, limit=100)
        # -- last change, 07/08/2011 -- writer = self.ix.writer()

        # Loop over the stored fields in the index
        count = 0
        for fields in searcher.all_stored_fields():
            indexed_path = fields['path']
            indexed_paths.add(indexed_path)
            err(count, '  \r')
            if not os.path.exists(indexed_path):
                # This file was deleted since it was indexed
                writer.delete_by_term('path', indexed_path)
                count -= 1
            else:
                # Check if this file was changed since it was indexed
                indexed_time = fields['time']
                mtime = os.path.getmtime(indexed_path)
                if mtime< 1:
                    continue
                    # that is usually a "bug" in the file

                if time.localtime(mtime) > indexed_time.timetuple():
                    # The file changed, delete it and add it to the list of files to reindex
                    writer.delete_by_term('path', indexed_path)
                    to_index.add(indexed_path)
                    count+=1
                else:
                    print 'time not changed'

        searcher.close()
        writer.commit()
        #writer.cancel()

        count = self.index_files( files_to_index=to_index, indexed_paths=indexed_paths)
        #writer.commit()
        #writer.close()

        return count



class Eyfo_Search(Eyfo_Base):

    def search(self, q, gui=False):
        '''  '''
        searcher = self.ix.searcher()
        q = unicode(q)

        query = QueryParser("body", self.ix.schema).parse( q )
        results = searcher.search(query, limit=MAX_RESULTS)
        out = ''
        for ret in results:
            p = ret.fields()['path']
            if gui:
                out+=p+'\n'
            else:
                print p

        query = QueryParser("filename", self.ix.schema).parse( unicode(q) )
        results = searcher.search(query, limit=MAX_RESULTS)
        for ret in results:
            p = ret.fields()['filename']
            if gui:
                out+=p+'\n'
            else:
                print p

        '''
        --------------- the correct way to combine queries! -------------
        # Parse the user query
        userquery = queryparser.parse(querystring)

        # Get the terms searched for
        termset = set()
        userquery.existing_terms(termset)

        # Formulate a "best bet" query for the terms the user
        # searched for in the "content" field
        bbq = Or([Term("bestbet", text) for fieldname, text
                  in termset if fieldname == "content"])

        # Find documents matching the searched for terms
        results = s.search(bbq, limit=5)

        # Find documents that match the original query
        allresults = s.search(userquery, limit=10)

        # Add the user query results on to the end of the "best bet"
        # results. If documents appear in both result sets, push them
        # to the top of the combined results.
        results.upgrade_and_extend(allresults)
        '''

        searcher.close()
        return out



def usage(s=''):
    print '''usage:

    '''
    print s
    sys.exit()





def process_cmd_line_args(argv):
    desc= ''' hello! '''
    epilog = ''' goodbye. '''

    parser = ArgumentParser( formatter_class=formatter, description=desc, epilog=epilog )
    parser.add_argument("--action", dest="action",                              default='', help='possible actions:   index (start a new index),  reindex (refresh existing index),  search (by query), test, info')
    parser.add_argument("--config-file", dest="config_file",                    default='', help="use specific config file")
    parser.add_argument("--log-file", dest="log_file",                          default='eyfo.log', help="use specific log file")
    parser.add_argument("--root-dir", dest="root_dir",                          default=None, help='root folder for importing images from')
    parser.add_argument("--index-db-dir", dest="index_db_dir",                  default=None, help='where the index_db is')
    parser.add_argument("--silent", action='store_true', dest='silent',         default=False, help="don't print progress details")
    parser.add_argument("--file-names", action='store_true', dest='only_fnames', default=False, help="search / index only names of files, not their contents")
    parser.add_argument('query', metavar='query', type=str, nargs='*',          default='', help='search query')


    results = parser.parse_args()
    args = dict(results._get_kwargs())
    if not args['action']:
        print "\nMUST have at least --action=xxxx !"
        parser.print_help()
        sys.exit(2)
    return args




# =======================================================

# --- defaults ---
# will be overritten with config file values

PLUGINS_SUB_DIR = '.'
TEST_DIR = PLUGINS_SUB_DIR
MAX_RESULTS = 2000
index_db_dir = 'c:\\temp'       # bad defaults
root_folder = 'C:\\'            # bad defaults
exclude_patterns = []

# --- enum of something ---
MAXSIZE=1
TEST=2
JUNK = 4
SIMPLE = 0

# --- that should probably go to a config file, too
exts = D({
    'css': D( { MAXSIZE: 0.5, TEST: u'background-url', JUNK: ['width', 'height', 'background', 'color', ], }),
    'doc': D( { MAXSIZE: 90, TEST: u'מסויים', JUNK: [ ], }),
    'docx': D( { MAXSIZE: 10, TEST: u'Saadat', JUNK: [ ], }),
    'htm': D( { MAXSIZE: 1.8, TEST: u'בדיקה', JUNK: [ ], }),
    'html': D( { MAXSIZE: 1.8, TEST: u'בדיקה', JUNK: [ ], }),
    'php': D( { MAXSIZE: 0.5, TEST: u'require', JUNK: ['foreach', 'case', 'true', 'false', 'include', 'for', ], }),
    'py': D( { MAXSIZE: 0.7, TEST: u'include', JUNK: ['def', 'from', 'type',  ], }),
    'txt': D( { MAXSIZE: 0.5, TEST: u'derigion', JUNK: [ ], }),
    'xls': D( { MAXSIZE: 4, TEST: u'macedonia', JUNK: [ ], }),
    'sql': D( { MAXSIZE: 4000, JUNK: ['table', 'update', 'select', 'join', 'create', 'drop', 'left', 'right', 'in', 'where', 'group', 'by', 'order', 'ignore', ], }),
    'zip': D( { MAXSIZE: 500, TEST: u'veronica', JUNK: ['readme', 'descript', 'ion', ], }),
})

exts['wbk'] = exts['doc'].copy()   # it's exactly the same format
parseable_exts = []
quick_hack_junk_words = ['for', 'as', 'at', 'is', 'are', 'was', 'lol', ':)', 'if',  ]

parser_functions = {}
load_plugins( True )





if __name__=='__main__':

    from eyfo_defaults import default_config_values, CONF_FILE_BASE_NAME
    from persistant_dict import persistant_dict
    import library
    import pprint
    pp = pprint.PrettyPrinter(indent=3)

    args = process_cmd_line_args(sys.argv)
    configfile = library.find_user_dir( particular_file=CONF_FILE_BASE_NAME, return_file_or_home='file', accept_current_dir=True, create_if_not_exist=True )
    conf = persistant_dict( configfile )
    conf.update_missing( default_config_values )

    conf.update_if_not_empty(args)

    action = conf['action']
    exclude_patterns = conf['exclude_patterns']

    if action in ['info', 'version']:
        print __doc__
        pp.pprint(conf)
        d = conf['index_db_dir']
        print 'index size: ', library.human_readable_numbers( library.dir_size( d ) )
        print 'index  dir: ', d
        sys.exit(0)


    if action in ['test', 't', ]:
        arg=''
        if len(sys.argv) > 2:
            arg = sys.argv[2]
        test_plugins(arg)
        sys.exit(0)

    for d in ['root_dir', 'index_db_dir']:
        if not conf[d]:
            print 'error: %s is empty. Please provide a valid folder (existing or new) on the command line!' % d
            sys.exit(4)


    if action in ['index', 'i']:
        if isfile(conf['index_db_dir']):
            usage(conf['index_db_dir'], " is a file, not a folder.")
        elif not isdir(conf['index_db_dir']):
            print 'dir not exist, but will be created'
        else:
            if not conf['index_db_dir']:
                usage("error: 'index_db_dir' in config file does not exist, and no index_db_dir provided in the command line")
        if not isdir(conf['root_folder']):
            usage("error - root folder not found")

        # remember for next time
        i = Eyfo_index( conf['index_db_dir'], clean=True )
        i.root = conf['root_folder']
        i.only_fnames = conf['only_fnames']
        i.index_files( indexed_paths = conf['root_folder'])
        sys.exit(0)


    elif action in ['reindex', 'r']:
        i = Eyfo_index( conf['index_db_dir'], clean=False )
        i.gui = False
        i.only_fnames = conf['only_fnames']
        i.incremental_index( root_dir = conf['root_folder'])


    elif action in ['s', 'search', 'find', ]:
        if not isdir(conf['root_dir']):
            print 'root dir not exist. where is your whoosh db?!'
            sys.exit(3)
        s = Eyfo_Search( conf['index_db_dir'] )
        s.only_fnames = conf['only_fnames']
        q = conf['query'].join(' ')
        s.search( q, gui=False )

    elif action in ['gui', 'search-gui', 'find-gui', ]:
        import EasyDialogs

        lastqf = find_user_dir( particular_file='whoosh-indexer-last-q.txt', return_file_or_home='file', accept_current_dir=False, create_if_not_exist=True )
        lastq = open(lastqf).read().decode('utf-8')

        args = ' '.join(sys.argv[2:]).strip()
        if not args: args = lastq
        q = EasyDialogs.AskString('search for what ?', default=args.encode('utf-8'))
        q=q.strip()

        if q:
            # -- woohoo! we got a query! ---
            open(lastqf, 'w').write(q)   # .encode('utf-8'))

            s = Eyfo_Search()
            txt = s.search( q, gui=True )
            html= filelist_to_html_index(txt.splitlines(), title=q, header1=q, subtitle="Eyfo indexer" )
            tmp = tempfile.mktemp(prefix='whoosh', suffix="-index.html")
            open(tmp,'wb').write(html.encode('utf-8', 'ascii'))
            cmd = 'start %s' % tmp
            os.system(cmd)


    else:  # user didn't provide us with valid command line
        usage()




