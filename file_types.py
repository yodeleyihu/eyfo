import sys
import os
import re
import urllib2
import urllib
import tempfile

JPEG_ID = '\xff\xd8\xff\xe0\x00'
PNG_ID = '\x89PNG'
GIF_ID = 'GIF89a'
pics = ['.gif', '.jpg','.jpeg','.gif','.png','.bmp','.tiff','.svg']


def match_extension_to_file_type(path, data=None):
    if not data:
        data = open(path, 'rb').read()
    parts = os.path.splitext(path)    
    ext = parts[1].lower()
    fname = parts[0]

    if is_jpeg(data) and ext not in ['.jpg', '.jpeg', '.jfif', '.jpe']:
        ext = '.jpg'
    if is_png(data) and ext not in ['.png']:
        ext = '.png'
    if is_gif(data) and ext not in ['.gif']:
        ext = '.gif'
    return fname + ext
        

        
def get_ext_for_single_file(f):
    return os.path.splitext(f)[1].lower()


def is_jpeg(data):
    pos = data.find(JPEG_ID)
    if pos> -1:
        if pos>0:
            print 'jpeg is encapsulated in piosition',pos
        return True
    return False

def is_png(data):
    pos = data.find(PNG_ID)
    if pos> -1:
        if pos>0:
            print 'PNG might be encapsulated in piosition',pos
        return True
    return False

def is_gif(data):
    pos = data.find(GIF_ID)
    return (pos> -1)


