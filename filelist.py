#coding: UTF8
"""
An subclass of wx.ListCtrl for displaying a list of files with icons.
Windows only.

Most of the list control code gleaned from the excellent wxpython demo.

Image code taken from this post by Mark Hammond:
http://mail.python.org/pipermail/python-win32/2005-March/003071.html

MIT license.
"""

__author__ = "Ryan Ginstrom"
__version__ = "0.1"

import wx
import sys, glob
import os

windows = sys.platform.startswith('win')
if windows:
    from win32com.shell import shell, shellcon
    from win32con import FILE_ATTRIBUTE_NORMAL

    def extension_to_bitmap(extension):
        """dot is mandatory in extension"""

        flags = shellcon.SHGFI_SMALLICON | \
                shellcon.SHGFI_ICON | \
                shellcon.SHGFI_USEFILEATTRIBUTES

        retval, info = shell.SHGetFileInfo(extension,
                                       FILE_ATTRIBUTE_NORMAL,
                                       flags)
        # non-zero on success
        assert retval

        hicon, iicon, attr, display_name, type_name = info

        # Get the bitmap
        icon = wx.EmptyIcon()
        icon.SetHandle(hicon)
        return wx.BitmapFromIcon(icon)

else:   #non windows version

    def extension_to_bitmap(extension):
        """dot is mandatory in extension"""

        # Get the bitmap
        icon = wx.EmptyIcon()
        #icon.SetHandle(hicon)
        return wx.BitmapFromIcon(icon)




class FileList(wx.ListCtrl):
    """Subclass of list control that shows a list of files with
    their file icons"""

    columns = [u"Name", u"Path"]

    def __init__(self, parent, id=-1):
        """style must be wx.LC_REPORT for now"""
        self.init_base(parent, id)

        self.extension_images = {}

    def init_base(self, parent, id):
        """Put this in a method so we can subclass
        and override for testing"""

        wx.ListCtrl.__init__(self, parent, id, style=wx.LC_REPORT,
                                pos=wx.Point(8, 224), size=wx.Size(360, 328) )

        for col, text in enumerate(self.columns):
            self.InsertColumn(col, text)

        for i in range(len(self.columns)):
            self.SetColumnWidth(i, wx.LIST_AUTOSIZE_USEHEADER)

        self.il = wx.ImageList(16,16, True)

    def get_image_id(self, extension):
        """Get the id in the image list for the extension.
        Will add the image if not there already"""

        # Caching
        if extension in self.extension_images:
            return self.extension_images[extension]

        bmp = extension_to_bitmap(extension)

        index = self.il.Add(bmp)
        self.SetImageList(self.il, wx.IMAGE_LIST_SMALL)

        self.extension_images[extension] = index

        return index

    def add_file(self, filename, fullnames=True, autowidth=False):
        """Add the filenames to the list, and returns the item index"""

        full_path = os.path.abspath(filename)
        path_part, file_part = os.path.split(full_path)

        # Add the icon
        extension = os.path.splitext(filename)[-1].lower()
        img_id = self.get_image_id(extension)

        # Add the file and path names
        #if fullnames:
        #    f = full_path
        #else:
        #    f = file_part
        
        index = self.InsertStringItem(sys.maxint, file_part, img_id)
        self.SetStringItem(index, 1, path_part)
        
        if autowidth:  # performance
            for i in range(len(self.columns)):
                self.SetColumnWidth(i, wx.LIST_AUTOSIZE_USEHEADER)

        return index

    def auto_width(self):
        for i in range(len(self.columns)):
            self.SetColumnWidth(i, wx.LIST_AUTOSIZE_USEHEADER)




class DemoFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1,
                          u"File List Demo",
                          size=(600,400))

        self.file_list = FileList(self)

        # add the rows
        for filename in sorted(glob.glob("*.*")):
            index = self.file_list.add_file(filename)


if __name__ == '__main__':
    os.chdir( os.path.dirname(__file__))

    app = wx.PySimpleApp(redirect=False, filename="log.txt", useBestVisual=True)
    frame = DemoFrame()
    frame.Show()
    app.MainLoop()

