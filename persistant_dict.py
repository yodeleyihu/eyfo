'''

persistant_dict.py

contact:    https://bitbucket.org/yodeleyihu/eyfo
license:    LGPL
created:    2013-02-03
version:    0.9 (beta)

description:
    a simple configuration file hack
    reasons:
        1. config file format should be readable by humans and kittens
        2. performance should be fast
        3. (no need to support more than 2-5kb)
        4. i want the changes to be written onChange, so in case the computer crashes, the settings so far are still saved (that might fuck up ACIDity, but i can live with it)

    why not (shelve, configobj, blah)
        i don't like their syntax
        they dont solve #4

    why not filedict?
        http://erez.wikidot.com/filedict-0-2-code
        erez is saving everything to mysql db. this is excelent! just doesn't answer #1, and i don't care about scalability, it's just a small settings file

    why not using a python module as settings file?
        i'd like to, but it is more difficult to save values this way, especially solving #4 above.


TODO:
    use python module as settings file
    or access keys via object-notation (yeah, some of you dont like it, but it's SO MUCH EASIER to code this way.
    prevent save()s during update and update_missing loops!

USAGE:

    >>> conf = persistant_dict(fname)

    that's it!
    your values are read from fname, if it exist,
    and if not, it is created now.

    >>> conf['x'] = 'yyy'

    that's it!
    the dictionary is saved in fname!

'''

from os.path import expanduser, isdir, isfile, join, basename, abspath, splitext
import json

class persistant_dict(dict):

    def __init__(self, fname):
        self.fname=fname
        self.can_save=True
        if not isfile(fname):
            open(fname,'w').write('{}')
        else:
            self.load()


    def __setitem__(self, key, value):
        x = super(persistant_dict, self).__setitem__(key, value)
        if self.can_save: self.save()
        return x

    def __delitem__(self, key):
        x = super(persistant_dict, self).__delitem__(key)
        self.save()
        return x

    def __missing__(self, key):
        return None

    def update(self, *args, **kwargs):
        self.can_save = False
        super(persistant_dict, self).update(*args, **kwargs)
        self.can_save = True
        self.save()


    def update_if_not_empty(self, d):
        ''' update values only if a value is neither None nor Empty String'''
        self.can_save = False
        for k in d:
            if d[k] is not None and d[k]!='':
                self[k] = d[k]
        self.can_save = True
        self.save()

    def update_missing(self, d):
        ''' update values only if they do not exist in object to update '''
        self.can_save
        for k in d:
            if k not in self:
                #self.__setitem__( k, d[k] )
                self[k] = d[k]
        self.can_save = True
        self.save()

    # --- additions to dict ------

    def save(self):
        s = json.dumps(self, sort_keys=True, indent=4)
        open(self.fname,'w').write(s)

    def load(self):
        try:
            self.update(json.loads(open(self.fname).read()))
        except:
            print "Raise error in file format (it must be a JSON dictionary). Fix the file manually or delete it, and try again"

