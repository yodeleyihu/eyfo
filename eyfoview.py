#Boa:Dialog:search_form
'''

EYFO - a local file indexer

started:    02/03/2013
contact:    https://bitbucket.org/yodeleyihu/eyfo
license:    LGPL
version:    0.4 (alpha)

This will need to be heavily reimplemented.

It was originally created by Boa Constructor, but is not compatible with it anymore, due to the use of "file_list" object. It would be nice to use Boa again, I just need to understand how to add file_list as a user-control...

Eyfo's constitution:
    - responsiveness - speed IS an issue
    - accessibility - the user interface must support keyboard shortcuts and shortcuts.
    - multi-platform
    - open source
    - multi-lingual:  detect and convert file's encoding, and convert its contents to unicode (utf8). I don't care much about the UI language, btw.
    - easy to read, write, maintain, and add plugins
    - plugins:  support extracting semantically meaningful parts of documents from as many file formats as needed.
    -

'''

from library import open_any_file, run
from os.path import expanduser, isdir, isfile, join, basename, abspath, splitext
from persistant_dict import persistant_dict
from wx import MessageBox as msgbox
from wx.lib.anchors import LayoutAnchors
import filelist
import os
import whoosh_indexer
import wx
import wx.animate
import wx.html
import wx.html2


def create(parent):
    return search_form(parent)

[wxID_SEARCH_FORM, wxID_SEARCH_FORMADD_PATH_BUTTON,
 wxID_SEARCH_FORMDEL_PATH_BUTTON, wxID_SEARCH_FORMDONT_SEARCH_WORDS_COMBO,
 wxID_SEARCH_FORMEXIT_BUTTON, wxID_SEARCH_FORMFILE_LIST,
 wxID_SEARCH_FORMGIFANIMATIONCTRL1, wxID_SEARCH_FORMHTML,
 wxID_SEARCH_FORMINDEX_BUTTON, wxID_SEARCH_FORMPATHS_LIST,
 wxID_SEARCH_FORMSEARCH_BUTTON, wxID_SEARCH_FORMSEARCH_WORDS_COMBO,
 wxID_SEARCH_FORMSELECT_ALL_PATH_BUTTON, wxID_SEARCH_FORMSPLITTERWINDOW1,
 wxID_SEARCH_FORMSTATICTEXT1, wxID_SEARCH_FORMSTATICTEXT2,
 wxID_SEARCH_FORMSTATUSBAR, wxID_SEARCH_FORMWHERE_TO_SEARCH,
] = [wx.NewId() for _init_ctrls in range(18)]


class search_form(wx.Dialog):
    def _init_coll_flexGridSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.staticText1, 2, border=0,
              flag=wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL)
        parent.AddWindow(self.search_words_combo, 6, border=0, flag=wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.index_button, 1, border=0,
              flag=wx.EXPAND | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER)
        parent.AddWindow(self.staticText2, 2, border=0,
              flag=wx.ALIGN_CENTER_VERTICAL)
        parent.AddWindow(self.dont_search_words_combo, 6, border=0,
              flag=wx.GROW | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.exit_button, 1, border=0,
              flag=wx.ALIGN_CENTER | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND)

    def _init_coll_flexGridSizer1_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(0)
        parent.AddGrowableCol(1)
        parent.AddGrowableCol(3)

    def _init_coll_paths_controls_sizer_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.add_path_button, 0, border=0,
              flag=wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER)
        parent.AddWindow(self.del_path_button, 0, border=0,
              flag=wx.ALIGN_CENTER | wx.ALIGN_CENTER_VERTICAL)
        parent.AddWindow(self.select_all_path_button, 0, border=0,
              flag=wx.ALIGN_CENTER | wx.ALIGN_CENTER_VERTICAL)

    def _init_coll_paths_selection_sizer_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.paths_list, 8, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.paths_controls_sizer, 2, border=0, flag=0)

    def _init_coll_boxSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.flexGridSizer1, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.boxSizer2, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.paths_selection_sizer, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.splitterWindow1, 6, border=3,
              flag=wx.EXPAND | wx.GROW)
        parent.AddWindow(self.statusbar, 0, border=0, flag=wx.GROW | wx.EXPAND)

    def _init_coll_boxSizer2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.where_to_search, 2, border=0,
              flag=wx.GROW | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.progress_indicator, 1, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.search_button, 1, border=0,
              flag=wx.GROW | wx.EXPAND)

    def _init_coll_statusbar_Fields(self, parent):
        # generated method, don't edit
        parent.SetFieldsCount(3)

        parent.SetStatusText(number=0, text=u'0 indexed files')
        parent.SetStatusText(number=1, text=u'0 files found')
        parent.SetStatusText(number=2, text=u'la la la')

        parent.SetStatusWidths([-1, -1, -1])

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self.flexGridSizer1 = wx.FlexGridSizer(cols=4, hgap=0, rows=2, vgap=0)
        self.flexGridSizer1.SetFlexibleDirection(wx.HORIZONTAL)
        self.flexGridSizer1.SetNonFlexibleGrowMode(wx.FLEX_GROWMODE_ALL)

        self.boxSizer2 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.paths_selection_sizer = wx.BoxSizer(orient=wx.HORIZONTAL)

        self.paths_controls_sizer = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)
        self._init_coll_flexGridSizer1_Items(self.flexGridSizer1)
        self._init_coll_flexGridSizer1_Growables(self.flexGridSizer1)
        self._init_coll_boxSizer2_Items(self.boxSizer2)
        self._init_coll_paths_selection_sizer_Items(self.paths_selection_sizer)
        self._init_coll_paths_controls_sizer_Items(self.paths_controls_sizer)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_SEARCH_FORM, name=u'search_form',
              parent=prnt, pos=wx.Point(258, 274), size=wx.Size(632, 473),
              style=wx.THICK_FRAME | wx.TAB_TRAVERSAL | wx.SYSTEM_MENU | wx.RAISED_BORDER | wx.RESIZE_BORDER | wx.MINIMIZE_BOX | wx.MAXIMIZE_BOX | wx.CLOSE_BOX | wx.CAPTION | wx.ALWAYS_SHOW_SB | wx.DEFAULT_DIALOG_STYLE,
              title=u'WhooSearch')
        self.SetClientSize(wx.Size(624, 446))
        self.Center(wx.VERTICAL)
        self.SetBackgroundStyle(wx.BG_STYLE_SYSTEM)

        self.search_words_combo = wx.ComboBox(choices=[], id=wxID_SEARCH_FORMSEARCH_WORDS_COMBO, name=u'search_words_combo', parent=self, pos=wx.Point(128, 8), size=wx.Size(362, 33), value=u'', style=wx.TAB_TRAVERSAL)
        self.search_words_combo.SetFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.NORMAL, False, u'Arial'))
        self.search_words_combo.SetLabel(u'')

        self.search_button = wx.Button(id=wxID_SEARCH_FORMSEARCH_BUTTON, label=u'&Search', name=u'search_button', parent=self, pos=wx.Point(507, 82), size=wx.Size(117, 70), style=wx.TAB_TRAVERSAL )
        self.search_button.Center(wx.VERTICAL)
        self.search_button.SetDefault()
        self.search_button.SetFont(wx.Font(20, wx.SWISS, wx.NORMAL, wx.BOLD, False, u'Arial Black'))
        self.search_button.Bind(wx.EVT_BUTTON, self.OnSearch_buttonButton, id=wxID_SEARCH_FORMSEARCH_BUTTON)

        self.index_button = wx.Button(id=wxID_SEARCH_FORMINDEX_BUTTON, label=u'&Index', name=u'index_button', parent=self, pos=wx.Point(498, 8), size=wx.Size(111, 33), style = wx.TAB_TRAVERSAL)
        self.index_button.Bind(wx.EVT_BUTTON, self.OnIndex_buttonButton, id=wxID_SEARCH_FORMINDEX_BUTTON)

        self.exit_button = wx.Button(id=wxID_SEARCH_FORMEXIT_BUTTON,
              label=u'E&xit', name=u'exit_button', parent=self,
              pos=wx.Point(498, 41), size=wx.Size(111, 33), style=wx.TAB_TRAVERSAL )
        self.exit_button.Bind(wx.EVT_BUTTON, self.OnExit_buttonButton,
              id=wxID_SEARCH_FORMEXIT_BUTTON)

        self.staticText2 = wx.StaticText(id=wxID_SEARCH_FORMSTATICTEXT2,
              label=u'does &NOT contain words ', name='staticText2',
              parent=self, pos=wx.Point(0, 50), size=wx.Size(128, 15),
              style=wx.ALIGN_RIGHT)

        #
        self.dont_search_words_combo = wx.ComboBox(choices=[], id=wxID_SEARCH_FORMDONT_SEARCH_WORDS_COMBO, name=u'dont_search_words_combo', parent=self, pos=wx.Point(128, 41), size=wx.Size(362, 33), style=wx.TAB_TRAVERSAL , value=u'')
        self.dont_search_words_combo.SetLabel(u'')

        self.where_to_search = wx.RadioBox(choices=[u'Search &File names only',
              u'Search also contents'], id=wxID_SEARCH_FORMWHERE_TO_SEARCH,
              label=u'How to search', majorDimension=1, name=u'where_to_search',
              parent=self, pos=wx.Point(0, 82), size=wx.Size(234, 70),
              style=wx.TAB_TRAVERSAL | wx.RA_SPECIFY_COLS)

        self.progress_indicator = wx.animate.GIFAnimationCtrl(filename=u'progress2.gif',
              id=wxID_SEARCH_FORMGIFANIMATIONCTRL1, name='progress_indicator',
              parent=self, pos=wx.Point(242, 82), size=wx.Size(257, 70),
              style=wx.animate.AN_FIT_ANIMATION|wx.NO_BORDER)

        self.splitterWindow1 = wx.SplitterWindow(id=wxID_SEARCH_FORMSPLITTERWINDOW1, name='splitterWindow1', parent=self, pos=wx.Point(0, 237), size=wx.Size(624, 188), style=wx.SP_3D)

        self.html = wx.html.HtmlWindow(id=wxID_SEARCH_FORMHTML, name=u'html', parent=self.splitterWindow1, pos=wx.Point(209, 2), size=wx.Size(413, 184), style=wx.TAB_TRAVERSAL | wx.html.HW_SCROLLBAR_AUTO)
        # self.html = wx.html2.WebView.New(id=wxID_SEARCH_FORMHTML, name=u'html', parent=self.splitterWindow1, pos=wx.Point(209, 2), size=wx.Size(413, 184), style=wx.html.HW_SCROLLBAR_AUTO | wx.TAB_TRAVERSAL)

        # self.file_list = wx.ListView(id=wxID_SEARCH_FORMFILE_LIST, name=u'file_list', parent=self.splitterWindow1, pos=wx.Point(2, 2), size=wx.Size(200, 184), style=wx.LC_ICON)
        # self.file_list = filelist.FileList(id=wxID_SEARCH_FORMFILE_LIST, parent=self)
        self.file_list = filelist.FileList(id=wxID_SEARCH_FORMFILE_LIST, parent=self.splitterWindow1)

        self.file_list.Bind(wx.EVT_KEY_UP, self.OnFile_listKeyUp)
        self.file_list.Bind(wx.EVT_LEFT_DCLICK, self.OnFile_listLeftDclick)
        self.file_list.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.OnFile_listListItemRightClick, id=wxID_SEARCH_FORMFILE_LIST)
        self.file_list.Bind(wx.EVT_LIST_ITEM_SELECTED,
        self.OnFile_listListItemSelected, id=wxID_SEARCH_FORMFILE_LIST)
        self.splitterWindow1.SplitVertically(self.file_list, self.html, 202)

        self.statusbar = wx.StatusBar(id=wxID_SEARCH_FORMSTATUSBAR,
              name=u'statusbar', parent=self, style=wx.CAPTION)
        self.statusbar.SetAutoLayout(True)
        self._init_coll_statusbar_Fields(self.statusbar)

        self.staticText1 = wx.StaticText(id=wxID_SEARCH_FORMSTATICTEXT1,
              label=u'Contain &Words ', name='staticText1', parent=self,
              pos=wx.Point(54, 17), size=wx.Size(74, 15), style=wx.ALIGN_RIGHT)

        self.paths_list = wx.CheckListBox(choices=[],
              id=wxID_SEARCH_FORMPATHS_LIST, name=u'paths_list', parent=self,
              pos=wx.Point(0, 160), size=wx.Size(488, 67), style=wx.TAB_TRAVERSAL)

        self.add_path_button = wx.Button(id=wxID_SEARCH_FORMADD_PATH_BUTTON,
              label=u'&Add', name=u'add_path_button', parent=self,
              pos=wx.Point(519, 160), size=wx.Size(75, 23), style=wx.TAB_TRAVERSAL)
        self.add_path_button.Bind(wx.EVT_BUTTON, self.OnAdd_path_buttonButton,
              id=wxID_SEARCH_FORMADD_PATH_BUTTON)

        self.del_path_button = wx.Button(id=wxID_SEARCH_FORMDEL_PATH_BUTTON,
              label=u'&Del', name=u'del_path_button', parent=self,
              pos=wx.Point(519, 183), size=wx.Size(75, 23), style=wx.TAB_TRAVERSAL)
        self.del_path_button.Bind(wx.EVT_BUTTON, self.OnDel_path_buttonButton,
              id=wxID_SEARCH_FORMDEL_PATH_BUTTON)

        self.select_all_path_button = wx.Button(id=wxID_SEARCH_FORMSELECT_ALL_PATH_BUTTON,
              label=u'Select Al&l', name=u'select_all_path_button', parent=self,
              pos=wx.Point(519, 206), size=wx.Size(75, 23), style=wx.TAB_TRAVERSAL)
        self.select_all_path_button.Bind(wx.EVT_BUTTON, self.OnSelect_all_path_buttonButton, id=wxID_SEARCH_FORMSELECT_ALL_PATH_BUTTON)

        self.Bind(wx.EVT_CHAR_HOOK, self.keybaord_handler)
        self.Bind(wx.EVT_MAXIMIZE, self.OnDialog1Maximize)
        self.Bind(wx.EVT_MOVE, self.OnDialog1Move)
        self.Bind(wx.EVT_SIZE, self.OnDialog1Size)

        self._init_sizers()

        self.order_tab_key


    def __init__(self, parent):
        self._init_ctrls(parent)
        self.init_eyfo()

    # to be overriden

    def OnDialog1Maximize(self, event):
        event.Skip()

    def OnDialog1Move(self, event):
        event.Skip()

    def OnDialog1Size(self, event):
        event.Skip()

    def OnAdd_path_buttonButton(self, event):
        event.Skip()

    def OnDel_path_buttonButton(self, event):
        event.Skip()

    def OnExit_buttonButton(self, event):
        event.Skip()

    def OnFile_listKeyUp(self, event):
        event.Skip()

    def OnFile_listLeftDclick(self, event):
        event.Skip()

    def OnFile_listListItemRightClick(self, event):
        event.Skip()

    def OnFile_listListItemSelected(self, event):
        event.Skip()

    def OnIndex_buttonButton(self, event):
        event.Skip()

    def OnSearch_buttonButton(self, event):
        event.Skip()

    def OnSelect_all_path_buttonButton(self, event):
        event.Skip()


if __name__ == '__main__':

    f = 'eyfo-config.json'
    configfile = find_user_dir( particular_file=f, return_file_or_home='file', accept_current_dir=True, create_if_not_exist=True )
    conf = persistant_dict( configfile )

    app = wx.PySimpleApp()
    dlg = create(None)
    #dlg.conf = conf
    try:
        dlg.ShowModal()
    finally:
        dlg.Destroy()
    app.MainLoop()
