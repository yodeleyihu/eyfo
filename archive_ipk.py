#import file_types
#def archive(filename, temp_dir)
file_type = 'ipk'

if sys.platform in ['win32']:
    os.windows_cmd = ''
    temp_dir = os.environ['TEMP']
if sys.platform in ['linux']:
    linux_cmd = ''
    temp_dir = '/tmp'

import tarfile

# 'filename' is from the parent scope
try:
    t = tarfile.TarFile.open(filename)
    found_files = t.getnames()
    #if 'control.tar.gz' in found_files:
    t.extract('control.tar.gz', temp_dir)   # not multiuser-friendly!
    t2 = tarfile.TarFile.open(temp_dir + os.sep + 'control.tar.gz')
    file_text = t2.extractfile('control').read()
    #if 'data.tar.gz' in found_files:
    t.extract('data.tar.gz', temp_dir)   # not multiuser-friendly!
    t2 = tarfile.TarFile.open(temp_dir + os.sep + 'data.tar.gz')
    found_files = t2.getnames()

except:
    print "(not an IPK (tar.gz) file? - %s)" % filename


