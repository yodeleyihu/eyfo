#import file_types
import library
from library import detect_encoding, err
import re

library.DEBUG= True
extensions = ['txt', ]
space = re.compile(r'\s+')

def get_contents(f, data):

    s = open(f, 'rb').read()
    enc = detect_encoding(s)
    if not enc:
        err('enc for file not ok?' + f)
        return data
    err('f:' + f + '  enc:' + enc)
    try:
        data.body = unicode(s, enc, 'replace')
    except LookupError, msg:
        err( u'decoding error: ' + unicode(msg) )
    except:
        data.body = ''
    data.body = space.sub(' ', data.body)

    return data

