# $f is the filename
# $d is the output dir (defaults to curr-dir). however, program should chdir to desired dir anyway because some doesnt support destination dir.
# $s is the path/file separator (/ or \)
#    win_cmd_list_filter = re.compile('''^ +[0-9]+ +[0-9]+ +[0-9.]+ +[0-9-]+ +[0-9:]+ +[0-9a-z]+ +[a-z-]+ +[BTPMGVX0-9\+]+ +$''', re.I)
#'^[^<>/\:*?"|]+ +[0-9]+ +[0-9]+ +[0-9.]+ +[0-9-]+ +[0-9:]+ +[0-9a-z]+ +[a-z-]+ +[BTPMGVX0-9\+]+ +$'
 
wincmd = '7z -y x $f $d$s'
wincmd_list = '7z l $f'
win_cmd_list_filter = re.compile('^[0-9][0-9-]+ [0-9:]+ ..... [0-9 ]+ [0-9 ]+ (.*?)$', re.M)
lincmd = 'un7z $f'
outtype = 'dir'


#if os.platfrm...
cmd = wincmd_list
cmd = prepare_command(cmd, filename, temp_dir)
#print cmd
txt = os.popen(cmd).read()
#print txt
found_files = win_cmd_list_filter.findall(txt)
#ret = os.system(cmd)
#print '7z exit status:', found_files
#sys.exit()
