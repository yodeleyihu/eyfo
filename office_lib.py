import re
import xlrd
import docx
import sys
import chardet
try:
    import win32com.client
    xlApp = win32com.client.Dispatch("Excel.Application")
except:
    xlApp = False


from library import decode_html_to_correct_encoding, html2text, is_unicode_file, SIMPLE
flags = re.I+re.M+re.S


def err(s):  
    sys.stderr.write(unicode(s) + u'\n')


def docx2text(f):
    d = docx.opendocx(f)
    text = '\n'.join( [ t for t in d.itertext()] )
    ## Print our documnts test with two newlines under each paragraph
    return unicode( text )


def excel_html_to_text(f):
    #todo
    #html=open(f).read()
    #html = decode_html_to_correct_encoding(html)
    return html2text(open(f).read())


def xls2tab(f, line_header=False):
    line = open(f).readline()
    if is_unicode_file(f, inspection_level=SIMPLE) or 'xmlns:o' in line or '<html' in line:
        #oops, this is a disguised html page
        return excel_html_to_text(f)
    try:
        wb = xlrd.open_workbook(f)
    except:  # struct.error, NameError; 
             #or check for file size (94152) not 512 + multiple of sector size (512)
        err('error in excel - probably old format!')
        return u''

    err('sheet_count='+str(len(wb.sheets())))

    lines=[]
    for sheet in wb.sheets():
        for r in range(sheet.nrows):
            if line_header:
                line = sheet.name
            else:
                line=''
            for c in range(sheet.ncols):
                val = sheet.cell_value(r,c)  
                if type(val)==type('a'):
                    val = val.replace('\n', '{NEWLINE}') 
                #if val not in ['',]:
                #    val = unicode( val  )
                val = unicode( val  )
                line += '\t' + val
            lines.append(line)

        lines.append('\n\n') # new sheet...

    return unicode('\n'.join(lines))
