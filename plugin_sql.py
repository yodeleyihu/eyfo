import re
#from html2text import html2text
from library import err, html2text, get_html_headers

extensions = ['sql', ]
namereg = re.compile('`[a-z0-9_]+?`', re.I+re.M+re.S)

def get_contents(f, data):
    html = open(f).read()
    data.body = u'\n'.join( namereg.findall(html) )

    return data

