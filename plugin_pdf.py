from library import read_stdout_from_cmd, err
import re, sys
import chardet

pdf2text = 'pdftotext'

extensions = ['pdf', ]

def using_calibre(f):
    #tempfile.txt
    cmd1 = 'C:\Program Files\Calibre2\ebook-convert.exe "%s" "%s"' % (f, tempfile)
    cmd2 = 'fribidi --nopad --nobreak ' + tempfile



try:
    from pdfminer.pdfparser import PDFDocument, PDFParser
    from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter, process_pdf
    from pdfminer.pdfdevice import PDFDevice, TagExtractor
    from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter
    from pdfminer.cmapdb import CMapDB
    from pdfminer.layout import LAParams
except:
    print 'error in plugin_pdf.py: module "pdfminer" is not found or err...'
    raise Error

reg_junk = re.compile(r'\(cid:[0-9]+\)', re.I+re.S+re.M)

# main
def get_contents(f, data):
    debug = 0
    password = ''
    pagenos = set()
    maxpages = 0
    outfile = None
    outtype = None
    outdir = None
    layoutmode = 'normal'
    codec = 'utf-8'
    pageno = 1
    scale = 1
    caching = True
    showpageno = True
    laparams = LAParams()
    PDFDocument.debug = debug
    PDFParser.debug = debug
    CMapDB.debug = debug
    PDFResourceManager.debug = debug
    PDFPageInterpreter.debug = debug
    PDFDevice.debug = debug
    #
    try:
        rsrcmgr = PDFResourceManager(caching=caching)
        if not outtype:
            outtype = 'text'
            if outfile:
                if outfile.endswith('.htm') or outfile.endswith('.html'):
                    outtype = 'html'
                elif outfile.endswith('.xml'):
                    outtype = 'xml'
                elif outfile.endswith('.tag'):
                    outtype = 'tag'
        import StringIO
        if outfile:
            outfp = file(outfile, 'w')
        else:
            #outfp = sys.stdout
            outfp = StringIO.StringIO()
        if outtype == 'text':
            device = TextConverter(rsrcmgr, outfp, codec=codec, laparams=laparams)
        elif outtype == 'xml':
            device = XMLConverter(rsrcmgr, outfp, codec=codec, laparams=laparams, outdir=outdir)
        elif outtype == 'html':
            device = HTMLConverter(rsrcmgr, outfp, codec=codec, scale=scale,
                                   layoutmode=layoutmode, laparams=laparams, outdir=outdir)
        elif outtype == 'tag':
            device = TagExtractor(rsrcmgr, outfp, codec=codec)
        else:
            return usage()
        #for fname in args:
        fname = f
        fp = file(fname, 'rb')
        process_pdf(rsrcmgr, device, fp, pagenos, maxpages=maxpages, password=password,
                    caching=caching, check_extractable=True)
        fp.close()
        device.close()
        s = outfp.getvalue()
        outfp.close()

        s = reg_junk.sub('', s)
        enc = chardet.detect(s)
        if enc:
            s = unicode(s, enc, 'replace')
        s = s.decode('iso-8859-8')
        data.body = s
    except:
        pass
    return data

if __name__ == '__main__':
    pdf2txt(sys.argv[1])

